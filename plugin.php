<?php
namespace Webforia_Cancel_Order;

class Plugin_Init
{

    public function __construct()
    {
        add_action('plugins_loaded', [$this, 'load_plugin_textdomain']);

    }


    /**
     * Load plugin domain pot
     *
     * @return void
     */
    public function load_plugin_textdomain()
    {
        load_plugin_textdomain(WEBFORIA_CANCEL_ORDERDOMAIN, false, dirname(plugin_basename(__FILE__)) . '/languages/');

    }

}