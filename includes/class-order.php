<?php

namespace Webforia_Cancel_Order;

class Order
{
    public function __construct()
    {
        add_action('woocommerce_cancel_unpaid_orders', 'cancel_order', 10);

    }

    /**
     * Get all order by date where status order on hold
     *
     * @return array id orders
     */
    public function get_order()
    {
        global $wpdb;

        $unpaid_submitted = array();

        if (get_option('wco_enabled')) {
            $date_option = get_option('wco_days', 3);

            $date_expired = date('Y-m-d H:i:s', strtotime("-{$date_option} days"));

            $unpaid_submitted = $wpdb->get_col($wpdb->prepare("
                SELECT posts.ID
                FROM {$wpdb->posts} AS posts
                WHERE posts.post_status = 'wc-on-hold'
                AND posts.post_date < %s
                ", $date_expired));
         }

        return $unpaid_submitted;
    }

    /**
     * Set order status to canceled if x time not paying by user
     *
     * @return void
     */
    public function cancel_order()
    {
        $unpaid_orders = $this->get_order();

        if ($unpaid_orders) {
            foreach ($unpaid_orders as $unpaid_order) {
                $order = wc_get_order($unpaid_order);

                if ($order->get_payment_method() == 'bacs') {
                    $order->update_status('cancelled', __('Unpaid order cancelled - time limit reached.', 'woocommerce'));
                }

            }
        }

    }

}
